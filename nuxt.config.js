export default defineNuxtConfig({
  modules: ['@nuxtjs/tailwindcss'],
  plugins: [
    { src: '~/assets/js/fontawesome.js' }
  ],
  axios: {
    baseURL: 'http://localhost:8000'
  },
});