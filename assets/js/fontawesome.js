import {library, config} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {
    faTrash as faTrash,
    faTimes as faTimes
} from '@fortawesome/free-solid-svg-icons'

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(
  faTrash,
  faTimes
)

export default defineNuxtPlugin(({vueApp}) => {
  vueApp.component('font-awesome-icon', FontAwesomeIcon)
})